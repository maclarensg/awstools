package main

import (
	"fmt"
	"regexp"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"gitlab.com/maclarensg/awstools/secrets"
)

func main() {
	svc := secretsmanager.New(session.New())
	var secretsList []*secretsmanager.SecretListEntry = secrets.ListAllSecrets(svc)

	for _, secret := range secretsList {
		if len(secret.SecretVersionsToStages) > 0 { //Secrets been initialized
			secretString := secrets.GetSecretString(svc, *secret.Name)
			matched, _ := regexp.MatchString(`.*dummy.*`, *secretString)
			if matched {
				fmt.Printf("%s: %s\n", *secret.Name, *secretString)
			}
			if *secretString == "{}" {
				fmt.Printf("%s: %s\n", *secret.Name, *secretString)
			}
		} else { // Print Uninitialized Secrets
			fmt.Println(*secret.Name)
		}
	}
}
