package secrets

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
)

func callAwsSecretsList(svc *secretsmanager.SecretsManager, input *secretsmanager.ListSecretsInput) *secretsmanager.ListSecretsOutput {
	result, err := svc.ListSecrets(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case secretsmanager.ErrCodeInvalidParameterException:
				fmt.Println(secretsmanager.ErrCodeInvalidParameterException, aerr.Error())
			case secretsmanager.ErrCodeInvalidNextTokenException:
				fmt.Println(secretsmanager.ErrCodeInvalidNextTokenException, aerr.Error())
			case secretsmanager.ErrCodeInternalServiceError:
				fmt.Println(secretsmanager.ErrCodeInternalServiceError, aerr.Error())
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(err.Error())
		}
		return nil
	}
	return result
}

// ListAllSecrets - List All Secrets and return a list of *secretsmanager.SecretListEntry
func ListAllSecrets(svc *secretsmanager.SecretsManager) []*secretsmanager.SecretListEntry {
	var secrets []*secretsmanager.SecretListEntry
	input := &secretsmanager.ListSecretsInput{}
	for {
		result := callAwsSecretsList(svc, input)
		secrets = append(secrets, result.SecretList...)
		// If there is next token set input with next token and continue loop
		// else no more result
		if result.NextToken != nil {
			input = &secretsmanager.ListSecretsInput{NextToken: result.NextToken}
		} else {
			break
		}
	}
	return secrets
}

// GetAllSecretName - iterate and return the list of secrets-manager name/id
func GetAllSecretName(svc *secretsmanager.SecretsManager) []string {
	var names []string
	secrets := ListAllSecrets(svc)
	for _, secret := range secrets {
		names = append(names, *secret.Name)
	}
	return names
}

// GetSecretString - Get secret string
func GetSecretString(svc *secretsmanager.SecretsManager, secretID string) *string {
	input := &secretsmanager.GetSecretValueInput{
		SecretId:     aws.String(secretID),
		VersionStage: aws.String("AWSCURRENT"), // We are only interested if the current version has values or not
	}

	result, err := svc.GetSecretValue(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case secretsmanager.ErrCodeResourceNotFoundException:
				fmt.Println(secretsmanager.ErrCodeResourceNotFoundException, aerr.Error())
			case secretsmanager.ErrCodeInvalidParameterException:
				fmt.Println(secretsmanager.ErrCodeInvalidParameterException, aerr.Error())
			case secretsmanager.ErrCodeInvalidRequestException:
				fmt.Println(secretsmanager.ErrCodeInvalidRequestException, aerr.Error())
			case secretsmanager.ErrCodeDecryptionFailure:
				fmt.Println(secretsmanager.ErrCodeDecryptionFailure, aerr.Error())
			case secretsmanager.ErrCodeInternalServiceError:
				fmt.Println(secretsmanager.ErrCodeInternalServiceError, aerr.Error())
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(err.Error())
		}
		return nil
	}

	return result.SecretString
}
